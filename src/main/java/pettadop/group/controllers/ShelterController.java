package pettadop.group.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import pettadop.group.data.repositories.PetRepository;
import pettadop.group.data.repositories.ShelterRepository;
import pettadop.group.models.Pet;
import pettadop.group.models.Shelter;

import java.util.ArrayList;
import java.util.Optional;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
public class ShelterController {

	String token = "supersecretkeytoken";

	Iterable<Shelter> lstShelters;

	Iterable<Pet> lstPets;

	@Autowired
	private ShelterRepository shelterRepository;

	@Autowired
	private PetRepository petRepository;

	public ShelterController() {

	}

	@GetMapping("/shelterpets")
	public ArrayList<Pet> pets(@RequestHeader("shelter") String shelter_id) {
		ArrayList<Pet> pets = new ArrayList<Pet>();
		// lstPets = petRepository.findAll();

		lstPets = petRepository.findByShelter(new Long(shelter_id));
		for (Pet pet : lstPets) {
			pets.add(pet);
		}

		if (!pets.isEmpty())
			return pets;
		else {
			return null;
		}
	}

	@GetMapping("/shelter")
	public Shelter shelter(@RequestHeader("shelter") String shelter_id) {
		//String tokenKey = "supersecretkeytoken";
		//ArrayList<Pet> pets = new ArrayList<Pet>();
		// lstPets = petRepository.findAll();

		Optional<Shelter> opShelter = shelterRepository.findById(new Long(shelter_id));
		
			
		if(opShelter.isPresent()) {
			// value is present inside Optional
			return opShelter.get();
		} else {
			// value is absent
			return null;
		}	
	}


	@PostMapping("/shelterregistration")
	@ResponseBody
	public String Register(HttpEntity<String> httpEntity)
	{
 		String jsonTwo = httpEntity.getBody();
 		
			lstShelters = shelterRepository.findAll();

		try 
		{
			Shelter registeredShelter = new ObjectMapper().readValue(jsonTwo, Shelter.class);
			shelterRepository.save(registeredShelter);
		} 
		catch (JsonMappingException e) 
		{
			e.printStackTrace();
		} 
		catch (JsonProcessingException e) 
		{
			e.printStackTrace();
		}

 		return "A new shelter has been registered.";
	}

	

 }
