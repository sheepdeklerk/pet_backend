package pettadop.group.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import pettadop.group.data.repositories.DogAgeRepository;
import pettadop.group.data.repositories.PetRepository;
import pettadop.group.data.repositories.PreferenceRepository;
import pettadop.group.data.repositories.RequestRepository;
import pettadop.group.data.repositories.UserRepository;
import pettadop.group.models.DogAge;
import pettadop.group.models.Pet;
import pettadop.group.models.Preference;
import pettadop.group.models.Request;
import pettadop.group.models.User;




@RestController
public class MainController {

	Iterable<Pet> lstPets;
	Iterable<DogAge> dogAgeList;
	@Autowired
	private PetRepository petRepository;

	@Autowired
	private RequestRepository requestRepository;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private DogAgeRepository dogAgeRepository;

	@Autowired
	private PreferenceRepository preferenceRepository;

	
	public MainController()
	{
				
	}
	
	@GetMapping("/pets")
	public ArrayList<Pet> pets(@RequestHeader("token") String token) 
	{
		String tokenKey = "supersecretkeytoken";
		ArrayList<Pet> pets = new ArrayList<Pet>();
		//lstPets = petRepository.findAll();
		
		lstPets = petRepository.findByAdopted(false);
		for (Pet pet : lstPets) {
			pets.add(pet);
		}
			
		if (token.equals(tokenKey))
			return pets;
		
		return null;
	}

	@GetMapping("/preferredpets")
	public ArrayList<Pet> preferredPets(@RequestHeader("id") String id) 
	{
		List<Preference> preferences = preferenceRepository.findByUserId(new Long(id));
		Preference preference;
		if(!preferences.isEmpty()){
			preference = preferences.get(0);	
			return petRepository.findByPreference(preference.petType_id, preference.petSize_id, preference.sex, preference.catFriendly, preference.dogFriendly);
		}

		return new ArrayList<Pet>();	
		
	}
	
	//private final static String QUEUE_NAME = "PetTaDopMQ";
	
	//Francois remember to add Message queue code here
	
	//this changes the adopted boolean to true if someone is intrested in the pet
	//we recieve the Id from front-end
	@PostMapping("/petadopted")
	@ResponseBody
	public void petadopt(HttpEntity<String> httpEntity) throws Exception
	{
		String adoptedString = httpEntity.getBody();
		
		if(lstPets==null)
		{
			lstPets = petRepository.findAll();
		}
		//System.out.println(adoptedString);
		
		String[] values = adoptedString.split(",");
		
		// long adoptedId = Long.parseLong(values[0]);
		// String adoptedUserName = values[1];
		// String adoptedUserCell = values[2];
		
		// String petAdoptedName = "";

		Long pet_id = Long.parseLong(values[0]);
		Long user_id = Long.parseLong(values[1]);
		String message = values[2];

		User u = userRepository.findById(user_id).get();
		Pet p = petRepository.findById(pet_id).get();
		
		Request rq = new Request(u.id, //user_id
								p.shelter_id, //shelter_id
								 p.id, //pet_id
								 u.firstName + " " + u.lastName, //user name
								 u.cell,
								 u.email,
								 p.petName,
                                 message
								 
								 );
		
		requestRepository.save(rq);
		// for(Pet item : lstPets)
		// {
		// 	if(item.id == adoptedId)
		// 	{
		// 		item.adopted = true;
		// 		petAdoptedName = item.petName;
		// 	}
		// }
		
		//this will be sent with a message queue => twillio



		String queueString = u.firstName + " " + u.lastName+","+ p.petName +","+u.cell;
		System.out.println(queueString);

		
		/*
		
		ConnectionFactory factory = new ConnectionFactory();
		factory.setHost("localhost");
		 try (Connection connection = factory.newConnection();
		      Channel channel = connection.createChannel()) 
		 {
		 	channel.queueDeclare(QUEUE_NAME, false, false, false, null);
		 	String messageTwo = queueString;
		 	channel.basicPublish("", QUEUE_NAME, null, messageTwo.getBytes());
		 	System.out.println(" [x] Sent '" + queueString + "'");
			

		 }

		*/
		
	}
	@PostMapping("/getDogAge")
	@ResponseBody
	public String dogAge(HttpEntity<String> httpEntity)
	{
		dogAgeList = dogAgeRepository.findAll();
		String s = "";

		for (DogAge dogAge : dogAgeList) {

			if(s.length()!=0)
			{
				s+=";";
			}

			s += dogAge.id + "," + dogAge.age;
		}

		if(s.length()> 0)
		{
			return s;
		}

		return null;
	}

	@PostMapping("/getDogBreed")
	@ResponseBody
	public String dogBreed(HttpEntity<String> httpEntity)
	{
		lstPets = petRepository.findAll();
		String s = "";

		for (Pet pet : lstPets) {
			if(s.length()!=0)
			{
				s+=";";
			}
			if (pet.petType_id == 1) //Dog
			s += pet.id + "," + pet.breed;
		}

		if(s.length()> 0)
		{
			return s;
		}

		return null;
	}

}
