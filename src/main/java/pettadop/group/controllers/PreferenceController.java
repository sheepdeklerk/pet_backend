package pettadop.group.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import pettadop.group.data.repositories.PreferenceRepository;
import pettadop.group.models.Preference;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
public class PreferenceController {

    @Autowired
	private PreferenceRepository preferenceRepository;

    @PostMapping("/registerpreference")
	@ResponseBody
	public String RegisterUser(HttpEntity<String> httpEntity)
	{

        String jsonTwo = httpEntity.getBody();

		try {
            Preference newPreference = new ObjectMapper().readValue(jsonTwo, Preference.class);
            
            preferenceRepository.save(newPreference);


        } catch (JsonMappingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (JsonProcessingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

		return "user reference created";
	}

}