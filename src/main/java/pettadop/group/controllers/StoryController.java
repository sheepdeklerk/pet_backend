package pettadop.group.controllers;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import pettadop.group.data.repositories.StoryRepository;
import pettadop.group.models.Story;

@RestController
public class StoryController {

	String token = "supersecretkeytoken";

	Iterable<Story> lstStories;


	@Autowired
	private StoryRepository storyRepository;

	

	public StoryController() {

    }

    @GetMapping("/recentstories")
	public ArrayList<Story> recentstories() {
        ArrayList<Story> stories = new ArrayList<Story>();
        // lstPets = petRepository.findAll();

		lstStories = storyRepository.findAllOrdered();
		int i = 0;
		for (Story story : lstStories) {
			if(i<3){
				stories.add(story);
				i++;
			}
		}

		
		if (!stories.isEmpty())
			return stories;
		else {
			return null;
		}
	}

	@GetMapping("/allstories")
	public ArrayList<Story> allstories() {
        ArrayList<Story> stories = new ArrayList<Story>();
        // lstPets = petRepository.findAll();

		lstStories = storyRepository.findAll();
		for (Story story : lstStories) {
			stories.add(story);
		}

		
		if (!stories.isEmpty())
			return stories;
		else {
			return null;
		}
	}
    
}