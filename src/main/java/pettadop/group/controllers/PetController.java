package pettadop.group.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import pettadop.group.data.repositories.PetRepository;
import pettadop.group.data.repositories.PetSizeRepository;
import pettadop.group.data.repositories.PetTypeRepository;
import pettadop.group.models.Pet;
import pettadop.group.models.PetSize;
import pettadop.group.models.PetType;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
public class PetController {
	
	String token = "supersecretkeytoken";
	
	Iterable<Pet> lstPets;
	Iterable<PetType> lstPetTypes;
	Iterable<PetSize> lstPetSizes;
    
	@Autowired
	private PetRepository petRepository;
	
	@Autowired
	private PetTypeRepository petTypeRepository;

	@Autowired
	private PetSizeRepository petSizeRepository;

	public PetController()
	{
		
	}
	
	
	@PostMapping("/petregistration")
	@ResponseBody
	public String RegisterPet(HttpEntity<String> httpEntity)
	{
 		String jsonTwo = httpEntity.getBody();
 		
			lstPets = petRepository.findAll();

		try 
		{
			Pet registeredPet = new ObjectMapper().readValue(jsonTwo, Pet.class);
			petRepository.save(registeredPet);
		} 
		catch (JsonMappingException e) 
		{
			e.printStackTrace();
		} 
		catch (JsonProcessingException e) 
		{
			e.printStackTrace();
		}

 		return "A new user has been registered.";
	}

	@PostMapping("/petTypes")
	@ResponseBody
	public String petTypes(HttpEntity<String> httpEntity) 
	{
		lstPetTypes = petTypeRepository.findAll();

		String s = "";
		
		for (PetType type : lstPetTypes) {
			
			if(s.length()!=0)
			{
				s+=";";
			}

			s += type.id + "," + type.type;
		}

		if(s.length()> 0)
		{
			return s;
		}

		return null;
	}

	@PostMapping("/petSizes")
	@ResponseBody
	public String petSizes(HttpEntity<String> httpEntity) 
	{
		lstPetSizes = petSizeRepository.findAll();

		String s = "";
		
		for (PetSize size : lstPetSizes) {
			
			if(s.length()!=0)
			{
				s+=";";
			}

			s += size.id + "," + size.size;
		}

		if(s.length()> 0)
		{
			return s;
		}

		return null;
	}


 }
