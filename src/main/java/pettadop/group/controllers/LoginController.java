package pettadop.group.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import pettadop.group.data.repositories.CityRepository;
import pettadop.group.data.repositories.UserRepository;
import pettadop.group.models.User;
import pettadop.group.models.City;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
public class LoginController {
	
	String token = "supersecretkeytoken";
	
	Iterable<User> lstUsers;
	Iterable<City> lstCities;

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
    private CityRepository cityRepository;
	
	public LoginController()
	{
		
	}
	
	@PostMapping("/login")
	@ResponseBody
	public String login(HttpEntity<String> httpEntity) 
	{

		String json = httpEntity.getBody();
		lstUsers = userRepository.findAll();
			
		try 
		{
			User logger = new ObjectMapper().readValue(json, User.class);
			for(User user : lstUsers)
			{
				if (user.email.equals(logger.email) && user.password.equals(logger.password))
				{			
					return token;
				}
			}
		} 
		catch (JsonMappingException e) 
		{
			e.printStackTrace();
		} 
		catch (JsonProcessingException e) 
		{
			e.printStackTrace();
		}
			
		
		return "nope";
	}
	
	
	//this is for when the user adopts a pet, token recieved in header + email + password recieved from body
	//method gives back a string with cell, and username
	@PostMapping("/getinfo")
	@ResponseBody
	public String users(@RequestHeader("token") String token, HttpEntity<String> httpEntity) 
	{
		String json = httpEntity.getBody();
		
		try 
		{
			User logger = new ObjectMapper().readValue(json, User.class);
			
			for(User user : lstUsers)
			{
				if (user.email.equals(logger.email) && user.password.equals(logger.password))
				{		
					return user.cell +","+ user.firstName + "," + user.lastName+ "," + user.email+ "," + user.shelter_id + "," + user.id + "," + user.userType_id;
				}
	
			}
		} 
		catch (JsonMappingException e) 
		{
			e.printStackTrace();
		} 
		catch (JsonProcessingException e) 
		{
			e.printStackTrace();
		}

		
		return null;
	}

	//this is for when the user wants to view their profile details
	//method gives back a string with all user details
	@PostMapping("/getUser")
	@ResponseBody
	public User profile(@RequestHeader("token") String token,@RequestHeader("id") String sId, HttpEntity<String> httpEntity) 
	{
		Long id = new Long(sId);
		//String json = httpEntity.getBody();
		return userRepository.findById(id).get();
	}

	@PostMapping("/updateuser")
	@ResponseBody
	public String UpdateUser(@RequestHeader("id") String sId,HttpEntity<String> httpEntity)
	{
		Long id = new Long(sId);
		String jsonTwo = httpEntity.getBody();
		User u;
		try {
			u = new ObjectMapper().readValue(jsonTwo, User.class);
			userRepository.updateUser(id, u.firstName, u.lastName, u.imageUrl, u.cell);
			return "User Updated";
		} catch (JsonMappingException e1) {
			e1.printStackTrace();
		} catch (JsonProcessingException e1) {
			e1.printStackTrace();
		}
		

			return "Error Updating User";

		

		
	}



	@PostMapping("/register")
	@ResponseBody
	public String RegisterUser(HttpEntity<String> httpEntity)
	{


		String jsonTwo = httpEntity.getBody();

			lstUsers = userRepository.findAll();

		try
		{
			User registerUserData = new ObjectMapper().readValue(jsonTwo, User.class);
			if (!registerUserData.password.matches("(?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{8,}"))
			{
				return "Password format incorrect";
			}

			for(User item : lstUsers)
			{
				if (registerUserData.email.equals(item.email))
					return "Warning: A user with this email has already been registered.";
			}
			registerUserData.userType_id = new Long(2);
			userRepository.save(registerUserData);
		}
		catch (JsonMappingException e)
		{
			e.printStackTrace();
		}
		catch (JsonProcessingException e)
		{
			e.printStackTrace();
		}

		return "A new user has been registered.";
	}

	@PostMapping("/getCities")
	@ResponseBody
	public String cities(HttpEntity<String> httpEntity) 
	{
		lstCities = cityRepository.findAll();
		String s = "";
		
		for (City city : lstCities) {
			
			if(s.length()!=0)
			{
				s+=";";
			}

			s += city.id + "," + city.name;
		}

		if(s.length()> 0)
		{
			return s;
		}

		return null;
	}

	// @PostMapping("/preferenceregister")
	// @ResponseBody
	// public String updateUserPreference(HttpEntity<String> httpEntity)
	// {


	// 	String jsonTwo = httpEntity.getBody();

	// 	lstUsers = userRepository.findAll();

	// 	try
	// 	{
	// 		User registerUserData = new ObjectMapper().readValue(jsonTwo, User.class);
	// 		for(User item : lstUsers)
	// 		{
	// 			if (item.id.equals(registerUserData.id)){
	// 				User current = userRepository.findById(item.id).get();
	// 				current.prefDogAge = registerUserData.prefDogAge;
	// 				current.catFriendly = registerUserData.catFriendly;
	// 				current.dogFriendly = registerUserData.dogFriendly;
	// 				current.petTypeId = registerUserData.petTypeId;
	// 				current.prefGender = registerUserData.prefGender;
	// 				current.prefPetSize = registerUserData.prefPetSize;
	// 				userRepository.deleteById(item.id);
	// 				userRepository.save(current);
	// 				System.out.println("USER UPDATED");
	// 				return "User Updated";
	// 			}
	// 		}
	// 	}
	// 	catch (JsonMappingException e)
	// 	{
	// 		e.printStackTrace();
	// 	}
	// 	catch (JsonProcessingException e)
	// 	{
	// 		e.printStackTrace();
	// 	}

	// 	return "A new user has been registered.";
	// }


	@PostMapping("/getCurrentUserId")
	@ResponseBody
	public Long getCurrentUserId(HttpEntity<String> httpEntity)
	{

		Long userId = null;
		String jsonTwo = httpEntity.getBody();
		lstUsers = userRepository.findAll();
		try
		{
			User userEmail = new ObjectMapper().readValue(jsonTwo, User.class);
			for(User item : lstUsers)
			{
				if (item.email.equals(userEmail.email)) {
					userId = item.id;
				}
			}
		}
		catch (JsonMappingException e)
		{
			e.printStackTrace();
		}
		catch (JsonProcessingException e)
		{
			e.printStackTrace();
		}

		return userId;
	}



}
