package pettadop.group.controllers;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import net.minidev.json.JSONObject;
import net.minidev.json.JSONValue;
import pettadop.group.data.repositories.ShelterRepository;
import pettadop.group.data.repositories.UserRepository;
import pettadop.group.models.JoinedShelter;
import pettadop.group.models.JoinedUser;
import pettadop.group.models.Shelter;




@RestController
public class AdminController {

	Iterable<JoinedUser> lstUsers;
	Iterable<Shelter> lstShelters;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private ShelterRepository shelterRepository;
	
	public AdminController()
	{
				
	}

	@PostMapping("/usershelter")
	@ResponseBody
	public String RegisterUser(HttpEntity<String> httpEntity)
	{
		String jsonTwo = httpEntity.getBody();
		JSONObject object = (JSONObject) JSONValue.parse(jsonTwo);

		Long id = Long.parseLong((String) object.get("user_id"));
		Long shelter_id = Long.parseLong((String) object.get("shelter_id"));


		userRepository.updateShelterById(id, shelter_id);

		return "User has been updated.";
	}

	

	
	@GetMapping("/getjoinedusers")
	public ArrayList<JoinedUser> pets(@RequestHeader("userType") Long userType) 
	{
        if (userType==null)
        {
            return null;
        }      
        if(userType!=1)
                return null;
            
		ArrayList<JoinedUser> users = new ArrayList<JoinedUser>();
		//lstPets = petRepository.findAll();
		ArrayList<Object> objects = userRepository.findJoinedUsers();
		

		for (Object object : objects) {
			JoinedUser u = new JoinedUser();
			Object[] obj = (Object[])object;
		
			u.id = (Long) obj[0];
			u.cell= (String) obj[1];
			u.city= (String) obj[2];
			u.email= (String) obj[3];
			u.firstName= (String) obj[4];
			u.lastName= (String) obj[5];
			u.password= (String) obj[6];
			u.shelter= (String) obj[7];
			u.imageUrl= (String) obj[8];
			u.type= (String) obj[9];
			
			users.add(u);
		}
			
		
		return users;
		
	}

	@GetMapping("/getshelters")
	public ArrayList<JoinedShelter> getShelters() 
	{
		return localJoinedShelters();
		
	}

	@GetMapping("/getjoinedshelters")
	public ArrayList<JoinedShelter> getJoinedShelters(@RequestHeader("userType") Long userType) 
	{
		if (userType==null)
        {
            return null;
        }       
        if(userType!=1)
                return null;
            
		
		return localJoinedShelters();
	}

	private ArrayList<JoinedShelter> localJoinedShelters(){
		ArrayList<JoinedShelter> shelters = new ArrayList<JoinedShelter>();
		ArrayList<Object> objects = shelterRepository.findJoinedShelters();
		

		for (Object object : objects) {
			JoinedShelter s = new JoinedShelter();
			Object[] obj = (Object[])object;
		
			s.id = (Long) obj[0];
			s.city= (String) obj[1];
			s.name= (String) obj[2];
			s.description= (String) obj[3];
			s.email= (String) obj[4];
			s.tel= (String) obj[5];
			s.website= (String) obj[6];
			s.logo= (String) obj[7];
			
			shelters.add(s);
		}

		return shelters;
	}

	@GetMapping("/test")
	public ArrayList<JoinedUser> test() 
	{
		ArrayList<JoinedUser> users = new ArrayList<JoinedUser>();
		//lstPets = petRepository.findAll();
		ArrayList<Object> objects = userRepository.findJoinedUsers();
		

		for (Object object : objects) {
			JoinedUser u = new JoinedUser();
			Object[] obj = (Object[])object;
		
			u.id = (Long) obj[0];
			u.cell= (String) obj[1];
			u.city= (String) obj[2];
			u.email= (String) obj[3];
			u.firstName= (String) obj[4];
			u.lastName= (String) obj[5];
			u.password= (String) obj[6];
			u.shelter= (String) obj[7];
			u.imageUrl= (String) obj[8];
			u.type= (String) obj[9];
			
			users.add(u);
		}
		
		
		// for (User user : lstUsers) {
		// 	users.add(user);
		// }
		return users;
		
	}
	
	

}
