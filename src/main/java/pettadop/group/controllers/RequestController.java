package pettadop.group.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import pettadop.group.data.repositories.RequestRepository;
import pettadop.group.models.Request;
import pettadop.group.models.Shelter;

import java.util.ArrayList;


@RestController
public class RequestController {

	String token = "supersecretkeytoken";

	Iterable<Request> lstRequests;

	Iterable<Shelter> lstShelters;

	@Autowired
	private RequestRepository requestRepository;

	public RequestController() {

	}

	@GetMapping("/shelterrequests")
	public ArrayList<Request> pets(@RequestHeader("shelter") String shelter_id) {
		ArrayList<Request> requests = new ArrayList<Request>();
		// lstPets = petRepository.findAll();

		lstRequests = requestRepository.findByShelter(new Long(shelter_id));
		for (Request request : lstRequests) {
			requests.add(request);
		}

		if (!requests.isEmpty())
			return requests;
		else {
			return null;
		}
	}



	

 }
