package pettadop.group.models;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@Entity
public class PetType {
  
    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", updatable = false, nullable = false)
	public Long id;
    public String type;
    
    
   
    public PetType(String type)
    {
        this.type = type;

    }

    public PetType()
    {
        
    }
}