package pettadop.group.models;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@Entity
public class User {


    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", updatable = false, nullable = false)

	public Long id;
    public String firstName;
    public String lastName;
    public String email;
    public String password;
    public String cell;
    @Column(columnDefinition  = "varchar(500)")
    public String imageUrl;
    //public String prefDogAge;
    //public boolean dogFriendly;
    //public boolean catFriendly;
    //public String prefGender;
    //public Long petTypeId;
    //public String prefPetSize;
    //getters and setters

    // @ManyToOne(fetch = FetchType.LAZY, optional = false)
    // @JoinColumn(name = "city_id", nullable = false)
    // @OnDelete(action = OnDeleteAction.CASCADE)
    // @JsonIgnore
    // private City city;
    
    @Column(name = "city_id")
    public Long city_id;

    @Column(name = "userType_id")
    public Long userType_id;

    @Column(name = "shelter_id" , updatable = true, nullable = true)
    public Long shelter_id;
    

    public User(String firstName, String lastName, String email,String password,String cell, Long city_id , Long userType_id, String imageUrl)
    {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.password = password;
        this.cell = cell;
        this.city_id = city_id;
        this.userType_id = userType_id;
        this.imageUrl = imageUrl;
        //this.shelter_id = shelter_id;
        //this.userType_id = new Long(2);
    }

    // public User(Long id, String prefDogAge, boolean dogFriendly, boolean catFriendly, String prefGender, Long petTypeId, String prefPetSize) {
    //     this.id = id;
    //     this.prefDogAge = prefDogAge;
    //     this.dogFriendly = dogFriendly;
    //     this.catFriendly = catFriendly;
    //     this.prefGender = prefGender;
    //     this.petTypeId = petTypeId;
    //     this.prefPetSize = prefPetSize;
    // }

    public void setShelter(Long shelter_id)
    {
        this.shelter_id = shelter_id;
    }

    public User()
    {

    }
}