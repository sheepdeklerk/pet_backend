package pettadop.group.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Pet {

    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", updatable = false, nullable = false)
	public Long id;
    public String petName;
    public String breed;
    public String sex;
    public int age;
    public String story;
    public String colour;
    public boolean adopted;
    public boolean catFriendly;
    public boolean dogFriendly;

    @Column(columnDefinition  = "varchar(500)")
    public String imageUrlOne;
    @Column(columnDefinition  = "varchar(500)")
    public String imageUrlTwo;
    @Column(columnDefinition  = "varchar(500)")
    public String imageUrlThree;

    @Column(name = "petType_id")
    public Long petType_id;

    @Column(name = "shelter_id")
    public Long shelter_id;

    @Column(name = "petSize_id")
    public Long petSize_id;


    //getters and setters

    public Pet(String firstName, String breed, String sex, int age, String story, String colour, boolean adopted,
    String imageUrlOne, String imageUrlTwo, String imageUrlThree,  Long petType_id, Long shelter_id, Long petSize_id) 
    {
    this.petName = firstName;
    this.breed = breed;
    this.sex = sex;
    this.age = age;
    this.story = story;
    this.colour = colour;
    this.adopted = adopted;
    this.imageUrlOne = imageUrlOne;
    this.imageUrlTwo = imageUrlTwo;
    this.imageUrlThree = imageUrlThree;
    //why need extras
    
    this.petType_id = petType_id;
    this.shelter_id = shelter_id;
    this.petSize_id = petSize_id;

    //##########################   CHANGE     ######################
    this.catFriendly = true;
    this.dogFriendly = true;
    }

    public Pet()
    {

    }

}