package pettadop.group.models;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@Entity
public class Shelter {
  
    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", updatable = false, nullable = false)
    public Long id;

    @Column(name = "city_id")
    public Long city_id;

    public String name;

    @Column(columnDefinition  = "varchar(1500)")
    public String description;

    public String email;

    public String tel;

    public String website;

    @Column(columnDefinition  = "varchar(500)")
    public String logo; 
   
    public Shelter(String name, String description, String email,String tel,String website, String logo, Long city_id)
    {
        this.name = name;
        this.description = description;
        this.email =email;
        this.tel=tel;
        this.website=website;
        this.logo=logo;
        this.city_id = city_id;
    }

    public Shelter()
    {
        
    }
}