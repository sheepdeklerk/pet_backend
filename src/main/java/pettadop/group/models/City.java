package pettadop.group.models;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@Entity
public class City {
  
    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", updatable = false, nullable = false)
	public Long id;
    public String name;
    public String description;
    
   
    public City(String name, String description)
    {
        this.name = name;
        this.description = description;
    }

    public City()
    {
        
    }
}