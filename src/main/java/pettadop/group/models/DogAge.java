package pettadop.group.models;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@Entity
public class DogAge {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    public Long id;
    public String age;



    public DogAge(Long id, String age)
    {
        this.id = id;
        this.age = age;

    }
    public DogAge(String age)
    {
        this.age = age;

    }
    public DogAge()
    {

    }
}