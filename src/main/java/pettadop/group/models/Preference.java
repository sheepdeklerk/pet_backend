package pettadop.group.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@Entity
public class Preference {
  
    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", updatable = false, nullable = false)
    public Long id;

    @Column(name = "user_id")
    public Long user_id;

    @Column(name = "petType_id")
    public Long petType_id;

    @Column(name = "petSize_id")
    public Long petSize_id;

    public String sex;

    public Boolean catFriendly;

    public Boolean dogFriendly;
   
    public Preference(Long user_id,Long petType_id, String sex, Boolean catFriendly,Boolean dogFriendly, Long petSize_id)
    {
        this.user_id = user_id;
        this.petSize_id = petSize_id;
        this.petType_id = petType_id;
        this.catFriendly = catFriendly;
        this.dogFriendly = dogFriendly;
        this.sex = sex;
    }

    public Preference()
    {

    }


}