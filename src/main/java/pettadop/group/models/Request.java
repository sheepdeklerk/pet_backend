package pettadop.group.models;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@Entity
public class Request {
  
    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", updatable = false, nullable = false)
    public Long id;

    @Column(name = "user_id")
    public Long user_id;

    @Column(name = "shelter_id")
    public Long shelter_id;

    @Column(name = "pet_id")
    public Long pet_id;

    public String userName;

    public String userCell;

    public String userEmail;

    public String petName;

    public Date date; 

    @Column(columnDefinition  = "varchar(1000)")
    public String message;
   
    public Request(Long user_id, Long shelter_id, Long pet_id, String userName,String userCell,String userEmail,String petName, String message)
    {
        this.user_id = user_id;
        this.shelter_id = shelter_id;
        this.pet_id = pet_id;
        this.userName = userName;
        this.userCell = userCell;
        this.userEmail = userEmail;
        this.petName = petName;
        this.date = new Date();
        this.message = message;
    }

    public Request()
    {
        
    }
}