package pettadop.group.models;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@Entity
public class Story {
  
    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", updatable = false, nullable = false)
    public Long id;

    @Column(name = "shelter_id")
    public Long shelter_id;

    public String title;

    @Column(columnDefinition  = "varchar(1500)")
    public String story;

    @Column(columnDefinition  = "varchar(500)")
    public String imageUrlOne;
    


    @Column(name = "city_id")
    public Long city_id;

    public String author;

    public Calendar date;
    
   
    public Story(String title, String story, String imageUrlOne,Long shelter_id, Long city_id, String author)
    {
        this.title = title;
        this.story = story;
        this.imageUrlOne=imageUrlOne;
        this.shelter_id=shelter_id;
        this.city_id = city_id;
        this.author = author;
        this.date = Calendar.getInstance();
    }

    public Story()
    {
        
    }
}