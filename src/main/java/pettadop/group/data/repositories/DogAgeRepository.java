package pettadop.group.data.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import pettadop.group.models.DogAge;

@Repository
public interface DogAgeRepository extends CrudRepository<DogAge, Long> {

}
