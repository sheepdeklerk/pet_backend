package pettadop.group.data.repositories;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import pettadop.group.models.Shelter;

@Repository
public interface ShelterRepository extends CrudRepository<Shelter, Long> {
    @Query("select s from Shelter s where s.id = :id")
    Optional<Shelter> findById(@Param("id") Long id);

    @Query("select s.id, c.name, s.name,  s.description,s.email, s.tel, s.website, s.logo from Shelter s join City c on c.id = s.city_id")
    ArrayList<Object> findJoinedShelters();
}