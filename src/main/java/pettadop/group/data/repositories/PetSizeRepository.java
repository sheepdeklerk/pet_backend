package pettadop.group.data.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import pettadop.group.models.PetSize;

@Repository
public interface PetSizeRepository extends CrudRepository<PetSize, Long> {
   
}