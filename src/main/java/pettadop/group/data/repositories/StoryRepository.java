package pettadop.group.data.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import pettadop.group.models.Story;

@Repository
public interface StoryRepository extends CrudRepository<Story, Long> {
    @Query("select s from Story s order by s.date desc")
    List<Story> findAllOrdered();
}