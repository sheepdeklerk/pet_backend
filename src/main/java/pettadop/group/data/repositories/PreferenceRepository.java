package pettadop.group.data.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import pettadop.group.models.Preference;

@Repository
public interface PreferenceRepository extends CrudRepository<Preference, Long> {
    @Query("select p from Preference p where p.user_id = :user_id")
    List<Preference> findByUserId( @Param("user_id") Long user_id);
}