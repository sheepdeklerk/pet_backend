package pettadop.group.data.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import pettadop.group.models.City;

@Repository
public interface CityRepository extends CrudRepository<City, Long> {
   
}