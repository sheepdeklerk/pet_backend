package pettadop.group.data.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import pettadop.group.models.Request;

@Repository
public interface RequestRepository extends CrudRepository<Request, Long> {
    @Query("select r from Request r where r.shelter_id = :shelter_id")
    List<Request> findByShelter( @Param("shelter_id") Long shelter_id);
}