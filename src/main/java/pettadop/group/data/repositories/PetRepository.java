package pettadop.group.data.repositories;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import pettadop.group.models.Pet;

@Repository
public interface PetRepository extends CrudRepository<Pet, Long> {
    @Query("select p from Pet p where p.adopted = :adopted")
    List<Pet> findByAdopted( @Param("adopted") Boolean adopted);

    @Query("select p from Pet p where p.shelter_id = :shelter_id")
    List<Pet> findByShelter( @Param("shelter_id") Long shelter_id);

    @Query("select p from Pet p where p.id = :id")
    Optional<Pet> findById(@Param("id") Long id); 


    @Query("select p from Pet p where p.petType_id = :petType_id and p.petSize_id = :petSize_id and p.sex = :sex and p.catFriendly = :catFriendly and p.dogFriendly = :dogFriendly")
    ArrayList<Pet> findByPreference( @Param("petType_id") Long petType_id,
                                @Param("petSize_id") Long petSize_id,
                                @Param("sex") String sex,
                                @Param("catFriendly") Boolean catFriendly,
                                @Param("dogFriendly") Boolean dogFriendly );

}