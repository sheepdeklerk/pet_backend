package pettadop.group.data.repositories;

import java.util.ArrayList;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import pettadop.group.models.User;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {
    @Query("select u from User u where u.id = :id")
    Optional<User> findById(@Param("id") Long id);

    // @Query("update user u set u.shelter_id = :shelter_id where u.id = :id")
    // void updateShelterById(@Param("id") Long id, Long shelter_id);

    // @Modifying(clearAutomatically = true)
    @Transactional
    @Modifying
    @Query("update User u set u.shelter_id =:shelter_id where u.id =:id")
    void updateShelterById(@Param("id") Long id, @Param("shelter_id") Long shelter_id);


    @Transactional
    @Modifying
    @Query("update User u set  u.firstName =:firstName, u.lastName =:lastName,  u.imageUrl =:imageUrl, u.cell =:cell  where u.id =:id")
    void updateUser(@Param("id") Long id, @Param("firstName") String firstName, @Param("lastName") String lastName, 
                   @Param("imageUrl") String imageUrl,@Param("cell") String cell);

    //email and password not needed above
    //@Query("update User u set  u.firstName =:firstName, u.lastName =:lastName, u.password =:password, u.imageUrl =:imageUrl, u.email =:email, u.cell =:cell  where u.id =:id")


    @Query("select u.id, u.cell, c.name, u.email, u.firstName, u.lastName, u.password, s.name, u.imageUrl, ut.type from User u join UserType ut on ut.id = u.userType_id join City c on c.id = u.city_id left join Shelter s on s.id = u.shelter_id")
    ArrayList<Object> findJoinedUsers();
}