package pettadop.group.data.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import pettadop.group.models.UserType;

@Repository
public interface UserTypeRepository extends CrudRepository<UserType, Long> {
   
}