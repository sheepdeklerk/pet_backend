package pettadop.group.data;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import pettadop.group.data.repositories.*;
import pettadop.group.models.*;
;

@Component
public class DataInit implements ApplicationRunner {

	private UserRepository userRepository;
    private PetRepository petRepository;
    private CityRepository cityRepository;
    private ShelterRepository shelterRepository;
    private PetTypeRepository petTypeRepository;
    private PetSizeRepository petSizeRepository;
    private UserTypeRepository userTypeRepository;
    private StoryRepository storyRepository;
    private RequestRepository requestRepository;
    private DogAgeRepository dogAgeRepository;
    private PreferenceRepository preferenceRepository;

    @Autowired
    public DataInit(UserRepository userRepository, PetRepository petRepository, CityRepository cityRepository,
            ShelterRepository shelterRepository, PetTypeRepository petTypeRepository,  PetSizeRepository petSizeRepository, 
            UserTypeRepository userTypeRepository, StoryRepository storyRepository, RequestRepository requestRepository, DogAgeRepository dogAgeRepository, PreferenceRepository preferenceRepository) {
        this.userRepository = userRepository;
        this.petRepository = petRepository;
        this.cityRepository = cityRepository;
        this.shelterRepository = shelterRepository;
        this.petTypeRepository = petTypeRepository;
        this.petSizeRepository = petSizeRepository;
        this.userTypeRepository = userTypeRepository;
        this.storyRepository = storyRepository;
        this.requestRepository = requestRepository;
        this.dogAgeRepository = dogAgeRepository;
        this.preferenceRepository = preferenceRepository;

    }

    public void run(ApplicationArguments args) throws Exception {
        if (dogAgeRepository.count() == 0) {
            DogAge dogAgeNoPref = new DogAge("Doesn't matter");
            DogAge dogAgePuppy = new DogAge("Young (2-12 Months)");
            DogAge dogAgeYoung = new DogAge("Young Adult (12-24 Months)");
            DogAge dogAgeMedium = new DogAge("Adult (2-8 Years)");
            DogAge dogAgeSenior = new DogAge("Senior (9+ Years)");

            dogAgeRepository.save(dogAgeNoPref);
            dogAgeRepository.save(dogAgePuppy);
            dogAgeRepository.save(dogAgeYoung);
            dogAgeRepository.save(dogAgeMedium);
            dogAgeRepository.save(dogAgeSenior);
        }
        if (cityRepository.count() == 0) {
            City cityOne = new City("Cape Town", "");
            City cityTwo = new City("Johannesburg", "");
            City cityThree = new City("Bloemfontein", "");
            cityRepository.save(cityOne);
            cityRepository.save(cityTwo);
            cityRepository.save(cityThree);
            if (shelterRepository.count() == 0) {
                Shelter shelterOne = new Shelter(   "African Tails",//name
                                                    "Once upon a time in December 2006, an organisation called African Tails was born. It is set on the "+
                                                    "dusty streets of townships in Cape Town, where African Tails strives to curb the over-population and "+
                                                    "suffering of abused and neglected township dogs.\n\n"+
                                                    "Everyone at African Tails is bound by their love for the uniquely African dog, known as Canis Africanis, "+ 
                                                    "frequently found in rural areas and on the streets of South Africa's informal settlements.", //desc
                                                    "adoptions@africantails.co.za",//email
                                                    "+27 21 510 7360", //number
                                                    "www.africantails.co.za", //website
                                                    "https://bit.ly/38D9tEE", //image
                                                     cityOne.id //city
                                                );

                Shelter shelterTwo = new Shelter(   "Kitty And Puppy Haven",//name
                                                    "Kitty and Puppy Haven is a pro-life sanctuary started in 2000 with the sole purpose of rescuing neglected, abused and abandoned animals. Most of our cases originate from veterinary practices, low income areas, other welfare organizations (that work in the townships and squatter-camps) or from emergency rescues. While animal rescue and rehabilitation is a priority, educating people on correct animal care is critical. \n" +
                                                            "\n" +
                                                            "We will not euthanase any animals if they can be healed and have a healthy, pain free, happy life. As most animals come from disadvantaged backgrounds, many require extensive veterinary care. Every animal also receives standard health care – vaccinations, de-worming, flea control and sterilisation. Our monthly veterinary bills average R40,000 – R50,000 per month.", //desc
                                                    "info@kittyhaven.co.za",//email
                                                    "010 224 0760", //number
                                                    "https://www.kittypuppyhaven.org.za/", //website
                                                    "https://www.kittypuppyhaven.org.za/wp-content/uploads/2019/07/Animal-Rescue-and-adoption-in-Johannesburg.jpg", //image
                                                    cityTwo.id  //city
                                                );
                Shelter shelterThree = new Shelter(   "Cape of Good Hope SPCA",//name
                        "Established in 1872, the Cape of Good Hope SPCA (Society for the Prevention of Cruelty to Animals) is the founding "+
                                "society of the SPCA movement in South Africa and is the oldest animal welfare organisation in the country.", //desc
                        "info@capespca.co.za",//email
                        "+27 21 700 4140", //number
                        "www.capespca.co.za", //website
                        "https://bit.ly/2TBzbFo", //image
                        cityOne.id  //city
                );
                Shelter shelterFour = new Shelter(   "Cape of Good Hope SPCA",//name
                        "Woodrock Animal Rescue was founded in 1992 by Nicholas and Stella (Estelle) Meldau, whose focus, passion and drive afforded " +
                                "their animal rescue vision to become a reality. The Animal Rescue Centre originated in the suburban area of Woodmead and Khyber Rock, hence the name Woodrock. " +
                                "We are now based on 8.5 hectares in the beautiful Hennops River Valley.", //desc
                        "adoptions@woodrockanimalrescue.co.za",//email
                        "076 155 4439", //number
                        "woodrockanimalrescue.co.za/", //website
                        "https://woodrockanimalrescue.co.za/wp-content/uploads/2019/12/logo-scroll-85x85.jpg", //image
                        cityTwo.id  //city
                );
                shelterRepository.save(shelterOne);
                shelterRepository.save(shelterTwo);
                shelterRepository.save(shelterThree);
                shelterRepository.save(shelterFour);

                if (petRepository.count() == 0) {

                    PetType petTypeDog = new PetType("Dog");
                    PetType petTypeCat = new PetType("Cat");
                    PetType petTypeRabbit = new PetType("Rabbit");
                    PetType petTypeHorse = new PetType("Horse");
                    PetType petTypeDonkey = new PetType("Donkey");
                    PetType petTypeBird = new PetType("Bird");

                    petTypeRepository.save(petTypeDog);
                    petTypeRepository.save(petTypeCat);
                    petTypeRepository.save(petTypeRabbit);
                    petTypeRepository.save(petTypeHorse);
                    petTypeRepository.save(petTypeDonkey);
                    petTypeRepository.save(petTypeBird);


                    PetSize petSizeOne = new PetSize("Small");
                    PetSize petSizeTwo = new PetSize("Medium");
                    PetSize petSizeThree = new PetSize("Large");

                    petSizeRepository.save(petSizeOne);
                    petSizeRepository.save(petSizeTwo);
                    petSizeRepository.save(petSizeThree);

                    Pet pet = new Pet();
                    pet.petName = "Daisy";
                    pet.breed = "Sheep dog";
                    pet.age = 1;
                    pet.sex = "Female";
                    pet.story = "Daisy is a fluffy sheep dog that comes from a long line of champions. Her father was a winning champion when it came to dog shows and tricks. Daisy has a very lovable character.";
                    pet.imageUrlOne = "https://d17fnq9dkz9hgj.cloudfront.net/breed-uploads/2018/08/old-english-sheepdog-detail.jpg?bust=1535566218&width=355";
                    pet.imageUrlTwo = "https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/old-english-sheepdog-1560758854.jpg?crop=0.668xw:1.00xh;0.133xw,0&resize=480:*";
                    pet.imageUrlThree = "https://images.ctfassets.net/w00x6p5qkejz/15Vje7zj4eiseAQqwYEgms/54ecd765b91789996031a26ccb691333/Old_English_Sheepdog1.png";
                    pet.adopted = false;
                    pet.catFriendly = true;
                    pet.dogFriendly = false;
                    pet.shelter_id = shelterThree.id;
                    pet.petType_id = petTypeDog.id;
                    pet.petSize_id = petSizeOne.id;

                    Pet petTwo = new Pet();
                    petTwo.petName = "Lucky";
                    petTwo.breed = "Jack Russel";
                    petTwo.age = 2;
                    petTwo.sex = "Male";
                    petTwo.story = "Lucky is a jack russel that comes from a long line of champions. His father was a winning champion when it came to dog shows and tricks. Lucky has a very lovable character.";
                    petTwo.imageUrlOne = "https://cdn1-www.dogtime.com/assets/uploads/2011/01/file_23080_jack-russell-terrier-460x290.jpg";
                    petTwo.imageUrlTwo = "https://vetstreet.brightspotcdn.com/dims4/default/f858016/2147483647/crop/0x0%2B0%2B0/resize/645x380/quality/90/?url=https%3A%2F%2Fvetstreet-brightspot.s3.amazonaws.com%2F52%2F86a4e0a7e511e0a0d50050568d634f%2Ffile%2FParson-Russell-Terrier-2-645mk062711.jpg";
                    petTwo.imageUrlThree = "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcStHZoYyeVJT5gwFlGGFy__XpR83JxxZxki-wcDA4DdM-ts-uY-";
                    petTwo.adopted = false;
                    petTwo.catFriendly = true;
                    petTwo.dogFriendly = true;
                    petTwo.shelter_id = shelterOne.id;
                    petTwo.petType_id = petTypeDog.id;
                    petTwo.petSize_id = petSizeTwo.id;

                    Pet petThree = new Pet();
                    petThree.petName = "Jessy";
                    petThree.breed = "Bulldog";
                    petThree.age = 1;
                    petThree.sex = "Male";
                    petThree.story = "Jessy is a fluffy bulldog that comes from a long line of champions. His father was a winning champion when it came to dog shows and tricks. Jessy has a very lovable character.";
                    petThree.imageUrlOne = "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQzDsoEWkZRBiM_94enMJ_ZnfEe6fKfIhm3V0zt26_ZCpBOa24W";
                    petThree.imageUrlTwo = "https://d17fnq9dkz9hgj.cloudfront.net/breed-uploads/2018/08/english-bulldog-detail.jpg?bust=1535565637&width=355";
                    petThree.imageUrlThree = "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcROm0ilqKUt59AggXwF773eTNdRCBGVWljCfDdtGcBzGXkkGgdX";
                    petThree.adopted = false;
                    petThree.catFriendly = false;
                    petThree.dogFriendly = false;
                    petThree.shelter_id = shelterOne.id;
                    petThree.petType_id = petTypeDog.id;
                    petThree.petSize_id = petSizeThree.id;

                    Pet petFour = new Pet();
                    petFour.petName = "Dozer";
                    petFour.breed = "Doberman";
                    petFour.age = 3;
                    petFour.sex = "Male";
                    petFour.story = "Jessy is a fluffy bulldog that comes from a long line of champions. His father was a winning champion when it came to dog shows and tricks. Jessy has a very lovable character.";
                    petFour.imageUrlOne = "https://www.dogzone.com/images/breeds/doberman.jpg";
                    petFour.imageUrlTwo = "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTMs303q1yh0XJetGCHhUpklo_8l_EOHAg099I-YL8q8Nfe8JwB";
                    petFour.imageUrlThree = "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQStBflOWqcCP7xjQZA4AzQj3Hd09cmqu8U3CMOaXbZTX_cjTMe";
                    petFour.adopted = true;
                    petFour.catFriendly = true;
                    petFour.dogFriendly = true;
                    petFour.shelter_id = shelterOne.id;
                    petFour.petType_id = petTypeDog.id;
                    petFour.petSize_id = petSizeThree.id;

                    Pet petFive = new Pet();
                    petFive.petName = "Shiloh";
                    petFive.breed = "Beagle X Boxer";
                    petFive.age = 1;
                    petFive.sex = "Female";
                    petFive.story = "She's a bubbly and bouncy lass who's just too adorable for words. She's such a friendly lovebug and is always ready with a wagging tail and floppy eared smile yo brighten your day. Don't miss out she's going to steal a whole bunch of hearts!";
                    petFive.imageUrlOne = "https://scontent.fgcj1-1.fna.fbcdn.net/v/t1.0-9/s960x960/89256483_2750369378351986_8498062716097789952_o.jpg?_nc_cat=105&_nc_sid=110474&_nc_ohc=VjBkZd1-HxgAX_WOfgc&_nc_ht=scontent.fgcj1-1.fna&_nc_tp=7&oh=b0a7bffaecd904ebde0a6f9358c402dc&oe=5E8DCE76";
                    petFive.imageUrlTwo = "https://scontent.fjnb10-1.fna.fbcdn.net/v/t1.0-9/s960x960/89149177_2750369728351951_4006625512691597312_o.jpg?_nc_cat=103&_nc_sid=110474&_nc_ohc=CtDqLTUo1-cAX_OX0JV&_nc_ht=scontent.fjnb10-1.fna&_nc_tp=7&oh=833abc942ea70698032fe21e3ada8b57&oe=5E906478";
                    petFive.imageUrlThree = "https://scontent.fjnb10-1.fna.fbcdn.net/v/t1.0-9/s960x960/89384608_2750370071685250_1501345405030367232_o.jpg?_nc_cat=106&_nc_sid=110474&_nc_ohc=NDTjRiapDaQAX9FjM8B&_nc_ht=scontent.fjnb10-1.fna&_nc_tp=7&oh=bf18b7ef7127972155682114b5b137bd&oe=5E91597E";
                    petFive.adopted = false;
                    petFive.catFriendly = false;
                    petFive.dogFriendly = true;
                    petFive.shelter_id = shelterTwo.id;
                    petFive.petType_id = petTypeDog.id;
                    petFive.petSize_id = petSizeTwo.id;

                    Pet petSix = new Pet();
                    petSix.petName = "Fudge";
                    petSix.breed = "Labrador X";
                    petSix.age = 1;
                    petSix.sex = "Female";
                    petSix.story = "She's a bubbly and bouncy lass who's just too adorable for words. She's such a friendly lovebug and is always ready with a wagging tail and floppy eared smile yo brighten your day. Don't miss out she's going to steal a whole bunch of hearts!";
                    petSix.imageUrlOne = "https://scontent.fgcj1-1.fna.fbcdn.net/v/t1.0-9/s960x960/89256483_2750369378351986_8498062716097789952_o.jpg?_nc_cat=105&_nc_sid=110474&_nc_ohc=VjBkZd1-HxgAX_WOfgc&_nc_ht=scontent.fgcj1-1.fna&_nc_tp=7&oh=b0a7bffaecd904ebde0a6f9358c402dc&oe=5E8DCE76";
                    petSix.imageUrlTwo = "https://scontent.fjnb10-1.fna.fbcdn.net/v/t1.0-9/s960x960/89149177_2750369728351951_4006625512691597312_o.jpg?_nc_cat=103&_nc_sid=110474&_nc_ohc=CtDqLTUo1-cAX_OX0JV&_nc_ht=scontent.fjnb10-1.fna&_nc_tp=7&oh=833abc942ea70698032fe21e3ada8b57&oe=5E906478";
                    petSix.imageUrlThree = "https://scontent.fjnb10-1.fna.fbcdn.net/v/t1.0-9/s960x960/89384608_2750370071685250_1501345405030367232_o.jpg?_nc_cat=106&_nc_sid=110474&_nc_ohc=NDTjRiapDaQAX9FjM8B&_nc_ht=scontent.fjnb10-1.fna&_nc_tp=7&oh=bf18b7ef7127972155682114b5b137bd&oe=5E91597E";
                    petSix.adopted = false;
                    petSix.catFriendly = true;
                    petSix.dogFriendly = true;
                    petSix.shelter_id = shelterTwo.id;
                    petSix.petType_id = petTypeDog.id;
                    petSix.petSize_id = petSizeTwo.id;

                    Pet petSeven = new Pet();
                    petSeven.petName = "Fudge";
                    petSeven.breed = "Labrador X";
                    petSeven.age = 1;
                    petSeven.sex = "Female";
                    petSeven.story = "She's a real sweetie pie who loves to give cuddles almost as much as she enjoys a good game of rough and tumble If you're looking for a bubbly and fun loving canine companion then Fudge is the one for you!";
                    petSeven.imageUrlOne = "https://scontent.fgcj1-1.fna.fbcdn.net/v/t1.0-9/s960x960/89435594_2746947818694142_2215298862321500160_o.jpg?_nc_cat=108&_nc_sid=8024bb&_nc_ohc=Y56ER7iqPAQAX_rni68&_nc_ht=scontent.fgcj1-1.fna&_nc_tp=7&oh=9a86c5dea7841d484b487a9f0c3f207b&oe=5EA68756";
                    petSeven.imageUrlTwo = "https://scontent.fgcj1-1.fna.fbcdn.net/v/t1.0-9/s960x960/88426603_2746948525360738_474270208882638848_o.jpg?_nc_cat=111&_nc_sid=8024bb&_nc_ohc=wYhCNS1WwS4AX-LJ_QQ&_nc_ht=scontent.fgcj1-1.fna&_nc_tp=7&oh=b9d5766eca50e96a6f5c3ab5e76cb4b1&oe=5E8E7B02";
                    petSeven.imageUrlThree = "https://scontent.fjnb10-1.fna.fbcdn.net/v/t1.0-9/s960x960/89384608_2750370071685250_1501345405030367232_o.jpg?_nc_cat=106&_nc_sid=110474&_nc_ohc=NDTjRiapDaQAX9FjM8B&_nc_ht=scontent.fjnb10-1.fna&_nc_tp=7&oh=bf18b7ef7127972155682114b5b137bd&oe=5E91597E";
                    petSeven.adopted = false;
                    petSeven.catFriendly = false;
                    petSeven.dogFriendly = false;
                    petSeven.shelter_id = shelterTwo.id;
                    petSeven.petType_id = petTypeDog.id;
                    petSeven.petSize_id = petSizeTwo.id;

                    Pet petEight = new Pet();
                    petEight.petName = "Teriyaki";
                    petEight.breed = "Bull Terrier X Alsation";
                    petEight.age = 1;
                    petEight.sex = "Male";
                    petEight.story = "He's one handsome lad, always on the lookout for fun, and can't wait to find a family of his own to share all life's adventures with.";
                    petEight.imageUrlOne = "https://scontent.fgcj1-1.fna.fbcdn.net/v/t1.0-9/s960x960/89263190_2744768562245401_320852134528548864_o.jpg?_nc_cat=101&_nc_sid=8024bb&_nc_ohc=ju654z4ZsnkAX9maToZ&_nc_ht=scontent.fgcj1-1.fna&_nc_tp=7&oh=c4d3bab5d44cc312b1d480c35571ffe9&oe=5E90BAE7";
                    petEight.imageUrlTwo = "https://scontent.fgcj1-1.fna.fbcdn.net/v/t1.0-9/s960x960/88336029_2744770562245201_4795434971437203456_o.jpg?_nc_cat=100&_nc_sid=8024bb&_nc_ohc=tklSnspi8L0AX-9_ghV&_nc_ht=scontent.fgcj1-1.fna&_nc_tp=7&oh=bbc9959b82ce219922b22198cc854758&oe=5E8D624D";
                    petEight.imageUrlThree = "https://scontent.fgcj1-1.fna.fbcdn.net/v/t1.0-9/s960x960/89567059_2744771712245086_8022354406135562240_o.jpg?_nc_cat=110&_nc_sid=8024bb&_nc_ohc=0shHij-CDVkAX8wCuCr&_nc_ht=scontent.fgcj1-1.fna&_nc_tp=7&oh=1072ab9fd9bb8820b26a56adee57051d&oe=5E8F0A3F";
                    petEight.adopted = false;
                    petEight.catFriendly = true;
                    petEight.dogFriendly = true;
                    petEight.shelter_id = shelterTwo.id;
                    petEight.petType_id = petTypeDog.id;
                    petEight.petSize_id = petSizeTwo.id;

                    Pet petNine = new Pet();
                    petNine.petName = "Zipper";
                    petNine.breed = "Shepherd mix";
                    petNine.age = 1;
                    petNine.sex = "Male";
                    petNine.story = "He's too cute for words and will have you firmly wrapped around his paw in no time at all. He's ready and waiting to audition for the role of your best buddy for life";
                    petNine.imageUrlOne = "https://scontent.fgcj1-1.fna.fbcdn.net/v/t1.0-9/s960x960/89385539_2742309695824621_5601487057428938752_o.jpg?_nc_cat=101&_nc_sid=8024bb&_nc_ohc=aQImYzSTWB4AX88Wq-v&_nc_ht=scontent.fgcj1-1.fna&_nc_tp=7&oh=75057395e5e8e84a391ed466ac46333a&oe=5E8D3EC2";
                    petNine.imageUrlTwo = "https://scontent.fgcj1-1.fna.fbcdn.net/v/t1.0-9/s960x960/88281272_2742311989157725_7640098502860603392_o.jpg?_nc_cat=111&_nc_sid=8024bb&_nc_ohc=LmSigTckFrIAX-bOgA6&_nc_ht=scontent.fgcj1-1.fna&_nc_tp=7&oh=c7c0cda12865dddd82bc8773278a0856&oe=5E8D8543";
                    petNine.imageUrlThree = "https://scontent.fgcj1-1.fna.fbcdn.net/v/t1.0-9/s960x960/89392276_2742311075824483_4474113165054967808_o.jpg?_nc_cat=109&_nc_sid=8024bb&_nc_ohc=b2rcvVnvNjkAX8F3mUq&_nc_ht=scontent.fgcj1-1.fna&_nc_tp=7&oh=d9c24af7a937c082026f6df8edc4c06c&oe=5EA6E479";
                    petNine.adopted = false;
                    petNine.catFriendly = false;
                    petNine.dogFriendly = false;
                    petNine.shelter_id = shelterTwo.id;
                    petNine.petType_id = petTypeDog.id;
                    petNine.petSize_id = petSizeThree.id;

                    Pet petTen = new Pet();
                    petTen.petName = "Thai";
                    petTen.breed = "Bull Terrier X Alsation";
                    petTen.age = 1;
                    petTen.sex = "Male";
                    petTen.story = "He's such a goofy and lovable lad that it won't take you long to fall for him hook, line and sinker";
                    petTen.imageUrlOne = "https://scontent.fgcj1-1.fna.fbcdn.net/v/t1.0-9/s960x960/88265898_2737466432975614_5177266643612467200_o.jpg?_nc_cat=109&_nc_sid=8024bb&_nc_ohc=7Zrx1-pWYkMAX8XYXcI&_nc_ht=scontent.fgcj1-1.fna&_nc_tp=7&oh=0246da97d7317fd9cc8a1dde251bed4f&oe=5EA3464E";
                    petTen.imageUrlTwo = "https://scontent.fgcj1-1.fna.fbcdn.net/v/t1.0-9/s960x960/89357869_2737466696308921_1340676519642529792_o.jpg?_nc_cat=104&_nc_sid=8024bb&_nc_ohc=9jnycED8vYMAX9P6lqY&_nc_ht=scontent.fgcj1-1.fna&_nc_tp=7&oh=4df257f33ad0b104d6a2a7023684a533&oe=5E8FAD76";
                    petTen.imageUrlThree = "https://scontent.fgcj1-1.fna.fbcdn.net/v/t1.0-9/p720x720/89298880_2737467139642210_4301253375774687232_o.jpg?_nc_cat=108&_nc_sid=8024bb&_nc_ohc=pcnuyCm8qT8AX_pB-Li&_nc_ht=scontent.fgcj1-1.fna&_nc_tp=6&oh=4580cc0d85c5a940066ec97429523d17&oe=5E8F311F";
                    petTen.adopted = false;
                    petTen.catFriendly = true;
                    petTen.dogFriendly = true;
                    petTen.shelter_id = shelterTwo.id;
                    petTen.petType_id = petTypeDog.id;
                    petTen.petSize_id = petSizeThree.id;

                    petRepository.save(pet);
                    petRepository.save(petTwo);
                    petRepository.save(petThree);
                    petRepository.save(petFour);
                    petRepository.save(petFive);
                    petRepository.save(petSix);
                    petRepository.save(petSeven);
                    petRepository.save(petEight);
                    petRepository.save(petNine);
                    petRepository.save(petTen);


                    if (userRepository.count() == 0) {

                        UserType adminType = new UserType("admin");
                        UserType userType = new UserType("user");

                        userTypeRepository.save(adminType);
                        userTypeRepository.save(userType);

                        User userOne = new User("Susan", "van Zyl", "susan0995@gmail.com", "1AmAN00b", "0794920995",
                                cityOne.id, adminType.id, "");
                        User userTwo = new User("Francois", "de Klerk", "sheep@gmail.com", "1AmAN00b", "0725138324",
                                cityTwo.id, userType.id, "");
                        User userThree = new User("John", "Smith", "guy@gmail.com", "1AmAN00b", "0725138324", cityThree.id,
                                userType.id, "");
                        userThree.setShelter(shelterOne.id);
                        userRepository.save(userOne);
                        userRepository.save(userTwo);
                        userRepository.save(userThree);

                        if (requestRepository.count() == 0) {
                            Request requestOne = new Request(userOne.id,

                                                             shelterOne.id,
                                                             petNine.id,
                                                             userOne.firstName + " " + userOne.lastName,
                                                             userOne.cell,
                                                             userOne.email,
                                                             petNine.petName,
                                                             "Is this pet still available?");


                            requestRepository.save(requestOne);
                        }

                        Preference prefOne = new Preference(userTwo.id, new Long(1), "Female", true, false, new Long(1));
                        preferenceRepository.save(prefOne);

                    }

                    if (storyRepository.count() == 0) {
                        Story storyOne = new Story("Phoenix's Story", //name
                                                   "This is baby Phoenix! I chose that name for a reason…he was owner surrendered to a "+ 
                                                   "shelter when he was only 11 weeks old in Belville and had 3 days to find a home or he would be euthanized. "+
                                                   "I couldn't get his sweet little face out of my head and I knew I had to save him! With the help of (we believe) "+
                                                   "his brothers' mom Lisa Ciotti, we adopted him and he made his way up to Stellenbosch. He had a bad case of demodex mange with a "+
                                                   "bacterial infection on top of that, but it wasn't anything that some love and medicine couldn't fix! He'll be a year old "+
                                                   "at the end of March and is healthy and demodex free! He's spoiled rotten and loves us unconditionally. We can't wait to "+
                                                   "rescue a sibling for him this summer!",//story
                                                   "https://barkpost.com/wp-content/uploads/2015/02/10959313_10203750189382903_2228331513502142938_n.jpg",//image
                                                   shelterOne.id, //shelter_id
                                                   cityOne.id, //city_id (CT)
                                                   "Danielle Baldwin") ; //author

                        Story storyTwo = new Story("Chad's Story", //name
                                                   "This is Chad's story: He and his mate, Clarisse, were abandoned at their house by their family. It wasn't just the two of them, though. "+
                                                   "There were bigger dogs and, in the fight for survival and to protect Clarisse, Chad was badly bitten by one of the other dogs and "+
                                                   "it quickly became infected. Luckily, a neighbor was aware of their situation and called animal control. While that call is not "+
                                                   "always a good one for most dogs, it was Chad's saving grace.\n"+
                                                   "People from the Georgia Jack Russell Adoption tag-teamed to save them from the shelter. "+
                                                   "Chad's infection was easily treated, but Clarisse's heartworms were too severe. She did not survive the treatments and left us "+
                                                   "all heartbroken, including Chad. Exactly one year ago our family decided to take him home with us to foster him and help him "+
                                                   "overcome his fears. Through all his former experiences he had developed a phobia of men and a big fear of strangers. He is still "+
                                                   "shy around strange men but has come a long way. In this past year he taught us the most. First how to handle a very fearful dog in "+
                                                   "different situations, but also how loving, giving and thankful a rescue dog is. And last but not least, that sometimes you have to give "+
                                                   "up fostering and admit, this living being is a part of you now and you will never be able to give him away, no matter how awesome another "+
                                                   "family could be. Thank you Chad for this amazing year full of laughter and fun, kisses and cuddles and most of all endless love. "+
                                                   "Happy Rescueversary! ",//story
                                                   "https://barkpost.com/wp-content/uploads/2015/02/10959835_553430318093002_2835826413831175797_n.jpg",//image
                                                   shelterOne.id, //shelter_id
                                                   cityOne.id, //city_id (CT)
                                                   "Isis Maria") ; //author
                        Story storyThree = new Story("Ms. Fang's story", //name
                                                   "Ms. Fang and I met the way all romantic comedies start. I was mourning the loss of my two cats, and she "+
                                                   "was newly single with a bad attitude that involved flirting unabashedly with anyone who gave her attention. "+
                                                   "We met next to a cash register where she was batting eyelashes at some employees, so I asked about her "+
                                                   "backstory. She was 11 years old, missing most of her teeth, and lost her home during a divorce.\n"+
                                                   "I learned that Fang had flirted with hundreds of people, but her age was a conversation stopper. To me, her age "+
                                                   "really only meant she knew what she wanted, which is to be a queen and she was unwilling to compromise. I was immediately drawn to her.\n"+
                                                   "I knew she was confused about the upheaval in her life and coping with it the best way she could; that I was in the midst of my "+
                                                   "own crisis and looking for inspiration, and that we were both lonely. So, Fang came home with me.",//story
                                                   "https://www.petcofoundation.org/wp-content/uploads/2019/11/Strawbridge_body2.jpg",//image
                                                   shelterOne.id, //shelter_id
                                                   cityOne.id, //city_id (CT)
                                                   "Nicole") ; //author


                        storyRepository.save(storyOne);
                        Story storyFour = new Story("Diana's story", //name
                                                    "Leon might be the funniest cat in the world. His joy and silliness radiate throughout our home and touch everyone he meets. Having a "+
                                                    "bad day? Leon can fix that. He'll put a smile on your face in no time, guaranteed. We lovingly refer to him as our little goofball, "+
                                                    "lovebug, jester or clown as he entertains us daily with his feline antics.\n"+
                                                    "Still a kitten at heart, Leon loves toys — whether it's his mousie-on-a-stick, stuffed catnip fishy, the purple spring he "+
                                                    "carries everywhere or, best yet, flinging this soggy unrecognizable furry thing (that ultimately ends up in our bed) into the "+
                                                    "air. Need a little respite from chores? Leon's got you covered. It may take longer to finish with him “helping” by inspecting "+
                                                    "the dryer and clean laundry, or bringing you that wet toy fresh from the water bowl while you're cleaning the litter box, but we "+
                                                    "wouldn't have it any other way.",//story
                                                    "https://www.petcofoundation.org/wp-content/uploads/2019/11/Clauss_body4.jpg",//image
                                                    shelterOne.id, //shelter_id
                                                    cityOne.id, //city_id (CT)
                                                    "Leon") ; //author

                        //storyOne.date =   ;              
                        storyOne.date.set(2020, 1, 1);
                        storyRepository.save(storyOne);
                        
                        storyRepository.save(storyTwo);

                        storyThree.date.set(2020, 2, 2);
                        
                        storyRepository.save(storyThree);

                        storyRepository.save(storyFour);

                        
                    }
                }

            }
        }

    }

}