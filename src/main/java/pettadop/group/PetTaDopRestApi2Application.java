package pettadop.group;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PetTaDopRestApi2Application {

	public static void main(String[] args) {
		SpringApplication.run(PetTaDopRestApi2Application.class, args);
	}

}
